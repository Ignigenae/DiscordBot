const Discord = require('discord.js');
const client = new Discord.Client();
const { TOKEN, PREFIX, GOOGLE_API_KEY } = require('./config');
var fetch = require("node-fetch");

var prefix = PREFIX;

//For music stuff
var servers = {};
var loop = false;
var Connection;

const ytdl = require('ytdl-core');
const YouTube = require('simple-youtube-api');
const youtube = new YouTube(GOOGLE_API_KEY);
const { google } = require('googleapis');

const getYoutubeID = require('get-youtube-id');
const fetchVideoInfo = require('youtube-info');
const request = require('request');

var videosrc;
var yturl1;
var yturl2 = "zpG1Ywmbioc";
var wholeurl;

var queue = [];
var isPlaying = false;
var dispatcher = null;
var voiceChannel = null;
var skipReq = 0;
var skippers = [];
var VideoInfo = [];

function now_playing(message) {
    try {
        fetchVideoInfo(queue[0], function (err, videoInfo) {
            if (err) throw new Error(err);
            VideoInfo = videoInfo;
        });
    } catch (error) { console.error(error); }
    if (VideoInfo.description === null) {
        VideoInfo.description = "No description was found.";
    }
    console.log(`Title: ${VideoInfo.title}\nURL: ${VideoInfo.url}\nDescription: ${VideoInfo.description.substring(0, 140)}\nThumbnail: ${VideoInfo.thumbnailUrl}\nDuration: ${VideoInfo.duration}`);
    var durMin = Math.floor(VideoInfo.duration / 60) % 60;
    var durSec = VideoInfo.duration % 60;
    if (durSec < 10) {
        durSec = "0" + durSec;
    }
    if (VideoInfo.duration > 3600) {
        var durHour = Math.floor(VideoInfo.duration / 3600);
        if (durMin < 10) {
            durMin = "0" + durMin;
        }
        var duration = `${durHour}:${durMin}:${durSec}`;
    } else {
        var duration = `${durMin}:${durSec}`;
    }
    console.log(`Formatted Duration: ${duration}`);
    message.channel.send({
        "embed": {
            "title": `${[VideoInfo.title]}`,
            "url": `${VideoInfo.url}`,
            "description": `${VideoInfo.description.substring(0, 140)}`,
            "thumbnail": {
                "url": `${VideoInfo.thumbnailUrl}`
            },
            "author": {
                "name": "Helperbot",
                "icon_url": "https://pics.me.me/english-muffin-5251601.png"
            },
            "fields": [
                {
                    "name": "Duration:",
                    "value": `${duration}`,
                    "inline": true
                },
                {
                    "name": "Views:",
                    "value": `${VideoInfo.views}`,
                    "inline": true
                },
                {
                    "name": "Likes/Dislikes:",
                    "value": `${VideoInfo.likeCount}/${VideoInfo.dislikeCount}`,
                    "inline": true
                },
                {
                    "name": "Genre:",
                    "value": `${VideoInfo.genre}`,
                    "inline": true
                }
            ]
        }
    });
}

function search_video(query, callback) {
    request("https://www.googleapis.com/youtube/v3/search?part=id&type=video&regionCode=US&q=" + encodeURIComponent(query) + "&key=" + GOOGLE_API_KEY, function (error, response, body) {
        var json = JSON.parse(body);
        callback(json.items[0].id.videoId);
    });
}

function getID(str, cb) {
    if (isYoutube(str)) {
        cb(getYoutubeID(str));
    } else {
        search_video(str, function (id) {
            cb(id);
        });
    }
}

function add_to_queue(strID) {
    if (isYoutube(strID)) {
        queue.push(getID(getYoutubeID(strID)));
    } else {
        queue.push(strID);
    }
}

function isYoutube(str) {
    return str.indexOf("youtube.com") > -1;
}

function playMusic(id, message) {
    voiceChannel = message.member.voiceChannel;

    voiceChannel.join().then(function (connection) {
        stream = ytdl("https://youtube.com/watch?v=" + id, {
            filter: "audioonly"
        });
        skipReq = 0;
        skippers = [];

        dispatcher = connection.playStream(stream);
        dispatcher.on('end', function () {
            skipReq = 0;
            skippers = [];
            if (!loop) {
                VideoInfo = [];
                queue.shift();
            }
            if (queue.length === 0) {
                queue = [];
                isPlaying = false;
                connection.disconnect();
            } else {
                try {
                    fetchVideoInfo(queue[0], function (err, videoInfo) {
                        if (err) throw new Error(err);
                        VideoInfo = videoInfo;
                        playMusic(queue[0], message);
                    });
                } catch (error) {
                    console.error(error);
                }
            }
            
        });
    })
}

function play(connection, message, args) {
    console.log(`Play: true`);
    console.log(`Connection: ` + connection);
    console.log(`Message: ${message}`);
    console.log(`Args: ` + args);
    var server = servers[message.guild.id];

    if (args[0].startsWith("https:")) {
        console.log("If == true");
        server.dispatcher = connection.playStream(ytdl(server.queue[0], { filter: "audioonly" }));
    } else {
        console.log("If == false");

        fetchlink(connection, message, args[0]);
        //if (yturl2 !== "zpG1Ywmbioc") 
            server.dispatcher = connection.playStream(ytdl(`${wholeurl}`, { filter: "audioonly" }));
    }

    
    if(!loop)
        server.queue.shift();
    server.dispatcher.on("end", function () {
        if (server.queue[0])
            play(connection, message);
        else if (loop)
            play(connection, message);
        else
            connection.disconnect();
    });
}

//End music stuff

//#region Voicelines
var fs = require('fs');

function voiceLine(connection, message, hero, args) {
    var server = servers[message.guild.id];
    
    if (hero !== "Soldier 76") {
        var templine = args[1];
        for (var i = 2; i < args.length; i++) {
            templine += " " + args[i];
        }
    } else {
        var templine = args[2];
        for (var i = 3; i < args.length; i++) {
            templine += " " + args[i];
        }
    }

    var URL = 'J:/JavaScript Programming/Discord Bot/voice lines/Overwatch/' + hero;
    var voiceline = templine + ".ogg";
    var Path = URL + "/" + voiceline;

    if (!fs.existsSync(Path)) {
        message.channel.send("The file could not be found, please check for any spelling mistakes");
        return;
    } 
    const dispatcher = connection.playFile(Path, { highWaterMark: 1 });
    
    console.log(`\n\nHero: ${hero}\nURL: ${URL}\nVoiceline: ${voiceline}\nPath: ${Path}\nDispatcher: ${dispatcher}`);

    dispatcher.on("end", function () {
        connection.disconnect();
    });
}
//#endregion

client.on('ready', () => {
    console.log('Ready!');
    client.user.setActivity('Helping people');
});

client.login(TOKEN);

client.on('userUpdate', user => {
    console.log("Hi");
})

client.on('message', message => {
    if (!message.content.startsWith(prefix) || message.author.bot) return;

    const args = message.content.slice(prefix.length).split(/ +/);
    const command = args.shift().toLowerCase();

    console.log(message.content);
    console.log(`Args: ${args}\nCommand: ${command}`);
    switch (command) {
        case `ping`: message.channel.send('pong!'); break;
        case `server`:
            try {
                message.channel.send(`Server name: ${message.guild.name}\nTotal members: ${message.guild.memberCount}\nCreated: ${message.guild.createdAt}\nRegion: ${message.guild.region}`);
            } catch (error) {
                message.channel.send(`There was an error executing your command... \nWould you like to see the error message for debugging? (y/n)`).then(() => {
                    const filter = m => message.author.id === m.author.id;

                    message.channel.awaitMessages(filter, { time: 60000, maxMatches: 1, errors: ['time'] })
                        .then(messages => {
                            console.log(`Message: ${messages.first().content}\n\n\n${error}`);

                            if (messages.first().content.startsWith("y")) {
                                console.log("If statement passed");
                                message.channel.send(`${error}`);
                            }
                        })
                        .catch(() => {
                            message.channel.send('You did not enter any input!');
                        });
                });
            }break;
        case `say`: message.channel.send(message.content.substring(prefix.length + command.length)); break;
        case `thank`:
            var taggedUser = message.mentions.users.first();
            if (args[0] === 'you') {
                message.reply("You're welcome! :D");
                message.react('😄');
            } else if (args[0] === `<@${taggedUser.id}>`) {
                message.channel.send(`${message.author} thanked you, ${taggedUser}`);
            }
            break;
        case `dinner`:
            var taggedUser = message.mentions.users.first();
            if (!message.mentions.users.size) {
                return message.channel.send(`Have a nice dinner, <@${message.author.id}>`);
            } else {
                return message.channel.send(`Have a nice dinner, ${taggedUser}`)
            }
            break;
        case `test`:
            message.channel.send(`it's working, ${message.author}!`);
            break;
        case `programming`:
            message.channel.send(`I was programmed with the help of https://discordjs.guide/#/`);
            break;
        case `help`:
            message.author.send("```The prefix to use HelperBot is hb!\nCommands:\n\nhb!ping - A basic command to test if the bot is active\nhb!server - Displays the server information\nhb!thank - Show someone your gratitude by thanking them, or thank helperbot :)\nhb!dinner - Going to eat dinner? HelberBot will wish you a nice dinner.\nhb!test - Another command to test if the bot is working\nhb!programming - Want to know how I was made? Learn it here and make your own bot! :O\nhb!who <am/is> <I/@mention> - Request information about yourself or someone else \nhb!message <@mention> - Send someone a DM \nhb!decide - Have no idea what to choose? Give helperbot all the options and he will decide for you```");
            break;
        case `who`:
            var taggedUser = message.mentions.users.first();
            if (args[0] === "am" && args[1] === "I?") {
                message.channel.send(`Your nickname is: ${message.author}\nYour ID number is: ${message.author.id}\nYou are in the channel: ${message.channel}`)
            } else if (args[0] === "is" && args[1] === `<@${taggedUser.id}>`) {
                message.channel.send(`Their nickname is: ${taggedUser}\nTheir ID number is: ${taggedUser.id}\nThey are in the channel: ${taggedUser.channel}`)
            }
            break;
        case `message`:
            var taggedUser = message.mentions.users.first();
            taggedUser.send(`${message.author.username} said: ` + message.content.substring(prefix.length + command.length + 18 + 5));
            console.log(message.author.username);
            console.log(`${message.author.username} said: ` + message.content.substring(prefix.length + command.length + taggedUser.length));
            break;
        case `decide`:
            message.channel.send(`${args[Math.floor((Math.random() * args.length))]}`);
            break;
        case `reset`:
            console.log(`ID: ${message.author.id}`);
            if (message.author.id === "370236248620662785")
                process.exit();
            else
                message.channel.send("You do not have the rights to reset helperbot");
            break;

        //Voice channel test
        case `join`:
            var voiceChannel = message.member.voiceChannel;

            voiceChannel.join()
            .then(connection => console.log('Connected!'))
            .catch(console.error);
            break;

        //#region Music commands
        case `play`:
            console.log("Play switch passed");
            if (queue.length > 0 || isPlaying) {
                getID(args, function (id) {
                    add_to_queue(id);
                    fetchVideoInfo(id, function (err, videoInfo) {
                        if (err) throw new Error(err);
                        message.reply("Added to queue: **" + videoInfo.title + "**");
                    });
                });
            } else {
                isPlaying = true;
                getID(args, function (id) {
                    queue.push("placeholder");
                    playMusic(id, message);
                    fetchVideoInfo(id, function (err, videoInfo) {
                        if (err) throw new Error(err);
                        message.reply("Now playing: **" + videoInfo.title + "**");
                        VideoInfo = videoInfo;
                    });
                });
            }
            //#region Deprecated if-statements PLEASE IGNORE
/*            if (!args[0]) {
                message.channel.send("Please provide a link");
                return;
            }
            //if (!args[0].startsWith("https:")) {
            //    message.channel.send("Unfortunately, a search function has yet to be implemented. \nYou can blame the lazy programmer who didn't want to stay up until 2 in the morning to figure it out");
            //    return;
            //}
            if (!message.member.voiceChannel) {
                message.channel.send("You must be in a voice channel for me to work");
                return;
            }
            if (!servers[message.guild.id])
                servers[message.guild.id] = {
                    queue: []
                };
            var server = servers[message.guild.id];
            server.queue.push(args[0]);

            if (!message.guild.voiceConnection)
                message.member.voiceChannel.join().then(function (connection) {
                    //fetchlink(args[0]);
                    play(connection, message, args);
                    Connection = connection;
                });
            else
                play(Connection, message, args);
*/
            //#endregion
            break;
        case `skip`:
            var server = servers[message.guild.id];

            if (dispatcher)
                dispatcher.end();
            break;
        case `np`:
            now_playing(message);
            break;
        case `stop`:
            var server = servers[message.guild.id];

            if (message.guild.voiceConnection) 
                message.guild.voiceConnection.disconnect();
            break;
        case `dis`:
            var server = servers[message.guild.id];
            message.guild.voiceConnection.disconnect();
            break;
        case `loop`:
            loop = !loop;
            if (loop)
                message.channel.send("Looping enabled");
            else
                message.channel.send("Looping disabled");
            break;
        //#endregion

        //Overwatch voice lines commands
        case `voiceline`:
            
            if (args[0] === "Bastion" || args[0] === "Sombra" || args[0] === "Orisa" || args[0] === "Brigitte" || args[0] === "Ana" || args[0] === "Moira" || args[0] === "Doomfist") {
                message.channel.send("I'm sorry, the unavailable heroes right now are Ana, Bastion, Brigitte, Doomfist, Moira, Orisa, Sombra");
            } else {
                if (!message.guild.voiceConnection)
                    message.member.voiceChannel.join().then(function (connection) {
                        switch(args[0]) {
                            case "D.va": voiceLine(connection, message, "D.Va", args); break;
                            case "Genji": voiceLine(connection, message, "Genji", args); break;
                            case "Hanzo": voiceLine(connection, message, "Hanzo", args); break;
                            case "Junkrat": voiceLine(connection, message, "Junkrat", args); break;
                            case "Lucio": voiceLine(connection, message, "Lucio", args); break;
                            case "McCree": voiceLine(connection, message, "McCree", args); break;
                            case "Mei": voiceLine(connection, message, "Mei", args); break;
                            case "Mercy": voiceLine(connection, message, "Mercy", args); break;
                            case "Pharah": voiceLine(connection, message, "Pharah", args); break;
                            case "Reaper": voiceLine(connection, message, "Reaper", args); break;
                            case "Reinhardt": voiceLine(connection, message, "Reinhardt", args); break;
                            case "Roadhog": voiceLine(connection, message, "Roadhog", args); break;
                            case "Soldier76": voiceLine(connection, message, "Soldier 76", args); break;
                            case "Symmetra": voiceLine(connection, message, "Symmetra", args); break;
                            case "Torbjorn": voiceLine(connection, message, "Torbjorn", args); break;
                            case "Tracer": voiceLine(connection, message, "Tracer", args); break;
                            case "Widowmaker": voiceLine(connection, message, "Widowmaker", args); break;
                            case "Winston": voiceLine(connection, message, "Winston", args); break;
                            case "Zarya": voiceLine(connection, message, "Zarya", args); break;
                            case "Zenyatta": voiceLine(connection, message, "Zenyatta", args); break;
                        }
                    });
                
            }
            break;
        //End Overwatch Voicelines
        case `convert`:
            var amount = args[0];
            switch (args[1]) {
                //#region Temperature
                case `C`:
                    switch (args[2]) {
                        case `F`:
                            console.log("Convert Celsius to Fahrenheit");
                            var output = amount * 9 / 5 + 32;
                            message.reply(`${amount} Celsius = ${output} Fahrenheit`);
                            break;
                        case `K`:
                            console.log("Convert Celsius to Kelvin");
                            var output = amount - 273;
                            message.reply(`${amount} Celsius = ${output} Kelvin`);
                            break;
                    }
                    break;
                case `F`:
                    switch (args[2]) {
                        case `C`:
                            console.log("Convert Fahrenheit to Celsius");
                            var output = (amount - 32) * 5 / 9;
                            message.reply(`${amount} Fahrenheit = ${output} Celsius`);
                            break;
                        case `K`:
                            console.log("Convert Fahrenheit to Kelvin");
                            var output = (amount - 32) * 5 / 9 - 273;
                            message.reply(`${amount} Fahrenheit = ${output} Kelvin`);
                            break;
                    }
                    break;
                case `K`:
                    switch (args[2]) {
                        case `C`:
                            console.log(`Convert Kelvin to Celsius`);
                            var output = amount + 273;
                            message.reply(`${amount} Kelvin = ${output} Celsius`);
                            break;
                        case `F`:
                            console.log(`Convert Kelvin to Fahrenheit`);
                            var output = (amount + 273) * 9 / 5 + 32;
                            message.reply(`${amount} Kelvin = ${output} Fahrenheit`);
                            break;
                    }
                    break;
                //#endregion
                //#region Mass
                case `kg`:
                    switch (args[2]) {
                        case `lbs`:
                            console.log(`Converting kg to lbs`);
                            var output = amount * 2.204622622;
                            message.reply(`${amount} kg = ${output} lbs`);
                            break;
                    }
                    break;
                //#endregion
                default:
                    message.reply(`I can't convert this yet, you'll have to wait until the programmer comes round to implementing it. \nOr, you can google it if you really want to know.`);
                    break;
            }
            break;
        default:
            message.channel.send(`The command ${command} was not recognised as a valid command. \nPlease use hb!help to see the list of possible commands.`);
            break;
    }
});